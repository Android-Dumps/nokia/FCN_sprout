#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_FCN_sprout.mk

COMMON_LUNCH_CHOICES := \
    lineage_FCN_sprout-user \
    lineage_FCN_sprout-userdebug \
    lineage_FCN_sprout-eng
